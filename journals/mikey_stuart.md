## Jan 4
Today we decided to go with PostgresSQL as our database. Forked our repo and created branches.
## Jan 5
Group coded to finish yaml file. Had some group issues with non-running container files. Resourced to some learn docs for a solution on students with M1 Mac chipsets. Also started making tables using PostgresSQL.
## Jan 6
Group coded to create a router and query file for users. Tested all endpoints using Swagger. Authentication is also working with login and logout.
## Jan 9
Group coded to get full CRUD for reviews. Created router and query file for our 3rd-party API, OMDB. Started with protecting a user's edits and was able to make it work. Tomorrow we are going to work on the delete.
## Jan 10
Group coded some response codes for better user feedback. Started on frontend authentication.
## Jan 11
Group coded to get frontend authentication to work. Started on navbar and got a login/logout modal to work.
## Jan 12
Group coded to get our home page to populate proper cards pulled from the API. Worked on making a search bar and signup modal.
## Jan 13
Group coded to get reviews from a user to populate on our detail view. Worked on implementing edit and delete buttons for our user's review.
## Jan 17
Over the weekend, Gavin had some ideas to make our edit/delete functions for reviews work without modals. Group coded some error messages and limitations for user feedback.
## Jan 18
Group coded tests individually and then everyone reviewed each other's tests to get them to work properly. Started on stretch goals with making a "favorites" page for our user's.
## Jan 19
Group coded our new "favorites" page and was able to implement a add and delete button.
## Jan 20
Group coded to touch up our page and brainstorm some new stretch goal ideas. Also added a count for all the times a movie was favorited. Once we finished we merged our dream branch into main to show favorites in our final project.
## Jan 23
Group coded to do some code organization.
## Jan 24
Group coded our README.md and ran a linter to organize our code.
## Jan 25
Group coded to try deployment and waited for instruction upon being stuck. Didn't get fully deployed, but did fix a lot of our problems.
## Jan 26
Group coded to work on deployment again and by the end of the day we were able to get both our front and back end deployed with help from Dalonte.
## Jan 27
Had an error with our deployment code trying to render locally. Fixed our code to show locally for presentation. Finished project and on to studying for exam.
