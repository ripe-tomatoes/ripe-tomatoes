## Jan 4

Today, I worked on:

1. As a team we worked on getting our repo shared to each member of the team.
2. We also spent time reading over Learn to see if we will pick MongolDB or SQL for our backend.

## Jan 5

Today, I worked on:

1. Today we did team coding and completed the yaml file. It appears to be working as we were able as Docker seems to be running.
2. I have run into an issue of one of my containers is not working. I have tried working with SEIRs and my teammates. Google has been a huge rabbit hole. I will call it a night and will try to find a solution tmw when I am better rested.

## Jan 6

Today, I worked on:

1. Today i worked with Jen and she was able to help me fix my bug. I had two things I had to do, first I had to enter a command before building my docker files. Second, i deleted my project in my computer, cloned it down again and that fixed the second bug.
2. Today did group coding and we were able to find get a new user working as well as a bearer token to work when a new user is created.

## Jan 9

Today, I worked on:

1. We have been cont with team coding and have made some progress. Our backend looks pretty close to being completed and we started to do some protection for our backend. Tmw we will start on the front wit some limited use to it.

## Jan 10

Today, I worked on:

1. Today it was my turn to drive for our Grp. We still cont to do Grp coding and as a team we are excited that we are not having any drama. Ok, maybe one. Mikey's name was inserted as Micky, we all had a laugh about it and Mikey was playfully upset about it.
2. We are getting really close to getting our AuthProvider to work. We into is populating but we are not happy that it is to long. We are going to try to tackle it tmw and we are able to start the front end.

## Jan 11

Today, I worked on:

1. It was Mikey's turn to be in the driver seat. We cont team coding and cont to make progress with Auth. We have the ability for a user to log in and log out and a bearer token is successful.
2. We did do some React and we had a few issues that seemed silly now. We did successfully install bootstrap, but we forgot to rebuild. Second was we forgot to import a CSS file into our app page and as soon as Candice came in she was able to see that issues and we fixed our last mistake.
3. Tmw we work on the Register Page.

## Jan 12

Today, I worked on:

1. Today we cont team coding and we cleared up a lot of the front end functions. We got the detail page mostly working, the search bar up and running, and now we have to start to fix a few cosmetic issues for those pages.

## Jan 13

Today, I worked on:

1. Today we cont with team coding and we fixed a lot of minor issues we had. We had issues yesterday with adding a review and today we got that working. A user can search for a movie and leave a review on the detail page.

## Jan 17

Today, I worked on:

1. We cont with Team coding. We fixed a few minor bugs and added some error messages. When a user logs in and has incomplete fields they get a message telling them that things are missing. We also added a Search message where a movie title has to meet the required length to be successful or they will also get an error message.

## Jan 18

Today, I worked on:

1. We worked on putting together our test and we had a few hiccups but we pulled together like a team we got all of our test to pass. We work great as a team and cont our team coding.
2. we feel pretty confident with the project and started to work on a new branch called Dream Branch with our stretch goals. We finished adding the backend for a Favorite, where a user can add a movie to their favorite list. We are still working on the front end of that feature.

## Jan 19

Today, I worked on:

1. We cont team coding and stretch goals, we worked on the favorites feature where a user can add it to their account and they can now see all their movie they have added and delete them as well.

## Jan 20

Today, I worked on:

1. We cont team coding and worked on stretched goals. We added some small easter eggs where a small loading screen appears during loading page, add some more things to detail page.

## Jan 23

Today, I worked on:

1. Today we look through our code to ensure that it was clean and had no issues.

## Jan 24

Today, I worked on:

1. We cont with team coding: We worked on the readme file, the tables, and formatting.
2. We also started to make videos of our project to show functionality. We will use this to make small gifs on our Readme file.

## Jan 25

Today, I worked on:

1. Team coding: today we started the deployment stage. We spent a lot of time waiting on getting answers from the instructors. And I mean we waited cause we are using a new deployment for our projects and had to wait from one instructor for instructions. We did get started on our gitlab-ci.yml and make a CapRover account. We ran into some issues and we will wait til Tmw and talk to Riley for some direction.

## Jan 26

Today, I worked on:

1. Team coding: We are still working on deploying. We spent all day working on it and waiting on teacher. We finally made some major advance towards the end. We had to change some CORS settings and that allowed the page to show some of our front page. This lead to us having log in issue, but we will tackle this tmw.

## Jan 27

Today, I worked on:

1. Team coding: We were able to get our deployment working. It was working last night but we did not know it. It takes a LONG time for the page to first load up. Once it does it works perfectly. We did add some padding to the search bar cause it looked bad on the deployment side and some minor things here and there, but we feel that it is working great and done for now.
2. We thought we were finish. We had to clear up some warnings in our console.
