## 1/27/23
Smoothed out the remaining bugs with deployment,
primarily one preventing local and online from running at the same time,
but ALL DONE!

## 1/26/23
After hours of working through bugs, we were able to properly
deploy, with now seemingly maybe a few minor bugs remaining.

## 1/25/23
Started the deployment process

## 1/24/23
Shared our screens and finished up working on the README.md;
ran linters through our code

## 1/23/23
Shared our screens and did some code organization

## 1/20/23
Pair coded and added the favorites count to the detail,
and added a loading animation

## 1/19/23
Pair coded and finished the favorites page, and add and
remove favorite on detail

## 1/18/23
Pair coded and completed making the favorites table and CRUD,
and made our 5 tests

## 1/17/23
Over the weekend, was able to sit down and complete
styling, and the detail page.
Today we pair coded and handled various error messages,
and organized

## 1/12/23
Pair coded and made various progress with the front end;
styling remains limited but near functionality completion

## 1/11/23
Pair coded and finished react front end authorization, and
a nav bar with limited styling

## 1/10/23
Pair coded and organized the react front end, and began to
work on basic functionality and authorization

## 1/9/23
Pair coded and got the full CRUD for the reviews, and made
progress on double layered protection for them

## 1/6/23
Pair coded and got the full CRUD, auth and token working
for the user

## 1/5/23
Pair coded the docker-compose.yaml, organized our project
and successfully made a table
